#!/usr/bin/env python

def dump():
    print '<?xml version="1.0" encoding="UTF-8"?>'
    print '<kml xmlns="http://www.opengis.net/kml/2.2">'
    print '<Document>'
    for line in open("/home/ceder/tmp/2012/Tankstallen.csv", "r"):
        line = line.strip()
        if line == '':
            continue
        parts = line.split(",", 2)
        desc = parts[2]
        if desc[0] == '"':
            desc = desc[1:]
        if desc[-1] == '"':
            desc = desc[:-1]
        descparts = desc.split(",")
        for i in range(len(descparts)):
            descparts[i] = descparts[i].strip()
        name = "CNG: " + descparts[0] + ", " + descparts[-1]
        description = ", ".join(descparts[1:-1])
        print ' <Placemark>'
        print '  <name>' + name + '</name>'
        print '  <description>' + description + '</description>'
        print '  <Point>'
        print '   <coordinates>' + parts[0] + "," + parts[1] + '</coordinates>'
        print '  </Point>'
        print ' </Placemark>'
    print '</Document>'
    print '</kml>'

if __name__ == '__main__':
    dump()
